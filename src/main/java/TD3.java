/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {

    if(a>b){
        return a;
    }
    else{
        return b;
    }

}

/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b , int c) {

    if (c>max2(a,b)){
        return c;
    }
    else{
        return max2(a,b);
    }
}


/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void repeteCarac(int nb, char car){
    // Action: Affiche nb caractère 'car' à partir de la position
    for(int i = 1; i < nb; i++){
        Ut.afficher(car);
    }
}

void pyramideSimple(int h, char c){
    // Action: Affiche une pyramide de heuteur h constitué de lignes répétant le caractère c
    for (int i = 1; i <= h; i++){
        for (int j = h; j > i; j--){
            Ut.afficher(" ");
        }
        repeteCarac(i*2,c);
        Ut.afficherSL("");
    }
}

void testPyramideSimple(){
    // Action: Demande à l'utilisateur une hauteur de pyramide et un caractère, puis affiche une pyramide
    Ut.afficher("Entrez la hauteur de la pyramide: ");
    int hauteur = Ut.saisirEntier();
    Ut.afficher("Entrez un caractère qui sera utilisé dans la pyramide: ");
    char car = Ut.saisirCaractere();
    pyramideSimple(hauteur, car);
}




/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){
    for (int i = 0; i <= nb2-nb1; i++){
        Ut.afficher((i+nb1)%10+" ");
    }

}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){
    for (int i = 0; i <= nb2-nb1; i++) {
        Ut.afficher((nb2 - i) % 10+ " ");
    }
}

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */

void pyramide(int h ) {
    int x=(h*2);
    for (int i = 1; i <= h; i++){
        repeteCarac(x,' ');
        afficheNombresCroissants(i,(2*i)-1);
        afficheNombresDecroissants(i,(2*i)-2);
        Ut.sautLigne();
        x=x-2;

    }

}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int nbChiffre(double x){
    int res=0;
    while(x>1){
        x=x/10;
        res++;
    }
    return res;
}
int nbChiffreCarre(double x){
    return nbChiffre((x*x));
}





boolean amis(int p, int q){
    int sommep=0;
    int sommeq=0;
    for (int i=1; i<p; i++) {
        if (p % i == 0) {
            sommep =sommep+i;
        }
    }
    for(int j=1; j<q; j++){
        if(q%j==0){
            sommeq =sommeq+j;
        }
    }
    if(p==sommeq && q==sommep){
        return true;
    }
    return false;
}

int racineParfaite(int c ){
    int n = (int)Math.sqrt(c);
    if(n*n==c){
        return n;
    }
    else{
        return -1;
    }

}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
    int x=racineParfaite(nb);
    if (x==(-1)){
        return false;
    }
    else{
        return true;
    }

}


boolean triangle(int x, int y){
    int z = (x*x)+(y*y);
    z=racineParfaite(z);
    if(z==-1){
        return false;
    }
    else{
        return true;
    }
}


boolean syracusien(int n, int nbr_op){
    for(int i=1; i<=nbr_op; i++){
        if(n%2==0){
            n=n/2;
        }
        else{
            n=3*n+1;
        }
    }
    if(n==1){
        return true;
    }
    return false;
}


public void main() {
    Ut.afficher(arrivobato(16,9));
}


//tp marin ivre//

int random(){
    return Ut.randomMinMax(1, 100);
}

int mouvement(){
    int aleatoire = random();
    if(aleatoire>=50){
        return 1;
    }
    else if (aleatoire>=30){
        return 2;
    }
    else if (aleatoire>=10){
        return 3;
    }
    else{
        return 4;
    }

}

boolean arrivobato(int plancheY, int plancheX ) {
    int n = 0;
    //position de base du marin //
    int posX = (plancheX + 1) / 2;
    int posY = 0;

    //        mouvement      //
    while (true) {
        int mouvement = mouvement();
        if (mouvement == 1) {
            posY = posY + 1;
            if (posY == plancheY) {
                return true;
            }
        } else if (mouvement == 2) {
            posX = posX + 1;
            if (posX > plancheX) {
                return false;
            }
        } else if (mouvement == 3) {
            posX = posX - 1;
            if (posX < 1) {
                return false;
            }
        } else {
            posY = posY - 1;
            if (posY < 0) {
                posX = (plancheX + 1) / 2;
                posY = 0;
            }
        }
        n++;
        Ut.afficherSL(posY + " " + posX);

    }


}

char affichePlanche(int x, int y, int mx, int my){

}





